## Prerequisites

Make sure you have the following installed on your local machine:

 - Docker: For building your container images and pushing them to a docker registry of choice.
 - Minikube: For running a local 1-node K8s cluster
 - Kubectl: For communicating with different k8s clusters
 
## Minikube setup

Boot minikube.

```
minikube start
```

You can check status of your minikube cluster by running,

```
minikube status
```

### Build and push the django container image

Assuming you are pushing "1.11" version.

#### Public registry

```
$ docker build -t lakshminp/django:1.11 . -f ./compose/local/django/Dockerfile 
```

```
$ docker push lakshminp/django:1.11
```

#### Gitlab registry

TODO


## Deploy new image

### Update the deployment spec

vi kubernetes/django.yml

```
    spec:
      containers:
        - name: django
          image: lakshminp/django:1.11
          ports:
            - containerPort: 8000
```

change the tag, in this case 1.11.

### Run deployment command

```
$ kubectl apply -f kubernetes/django.yml
```

## Common scenarios


### Scaling up/down

Change the number of replicas in the deployment spec.

```
spec:
  replicas: 3
```

and rerun deployment.

```
$ kubectl apply -f kubernetes/django.yml
```

## OpenShift instructions

### S2i image

```
$ docker build -t lakshminp/django-alpine-s2i:1.0 .
$ docker push lakshminp/django-alpine-s2i:1.0
```

```
$ oc import-image lakshminp/django-alpine-s2i:1.0 --confirm
```


```
oc create secret generic badri1 \
    --from-literal=username=badri1 \
    --from-literal=password=xxx \
    --type=kubernetes.io/basic-auth
```

```
oc secrets link builder badri1
```

```
 oc annotate secret/badri1 \
    'build.openshift.io/source-secret-match-uri-1=https://gitlab.com/badri1/django-k8s-demo.git'
```

### nginx image

```
docker build -t lakshminp/django-nginx:1.1 .
```
```
docker push lakshminp/django-nginx:1.1
```

